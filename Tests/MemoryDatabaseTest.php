<?php

use PHPUnit\Framework\TestCase;

class MemoryDatabaseTest extends TestCase
{
    protected $pdo;

    public function setUp(): void
    {
        $this->pdo = new PDO('sqlite::memory:');
        $this->pdo->exec('CREATE TABLE articles (id INTEGER PRIMARY KEY, description TEXT)');
    }
    // ... Rest of your setup logic

    public function testInsertData(): void
    {
        $this->assertEquals(1, $this->insertLineIndb());
    }
    
    public function testQueryData() : void
    {
        $this->insertLineIndb();
        $stmt = $this->pdo->query('SELECT * FROM articles');
        $users = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $this->assertEquals('chocolat', $users[0]['description']);
    }

    private function insertLineIndb() {
        $stmt = $this->pdo->prepare('INSERT INTO articles (description) VALUES (:description)');
        $stmt->execute([':description' => 'chocolat']);
        return $stmt->rowCount();
    }

}