<?php

use PHPUnit\Framework\TestCase;

class PagesIntegrationTest extends TestCase
{

    public function testExistingPage(): void
    {
        $ok = 0;
        $message = "The page exists";
        try {
            require_once (__DIR__ ."/../pages/Articles/chocolat.php"); 
            echo $message; 
            $ok = 1;
        } catch (Exception $e) {
            echo $e->getMessage();
            $message= $e->getMessage();
        }
        $this->assertEquals(1, $ok, $message);      
    }

    public function testUnExistingPage(): void
    {
        $ok = 0;
        $message = "The page exists";
        try {
            include (__DIR__."/../pages/Articles/voiture.php");  
            echo $message;
            $ok = 1;
        } catch (Exception $e) {
            echo $e->getMessage();
            $message= $e->getMessage();
        }
        $this->assertNotEquals(1, $ok, $message);      
    }
}
