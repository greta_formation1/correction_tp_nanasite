FROM php:7.4-apache
COPY . /var/www/html/
# symlink
RUN pwd
RUN chmod 0775 vendor/bin/phpunit

# the actual executable
RUN chmod 0775 vendor/phpunit/phpunit/phpunit
RUN docker-php-ext-install mysqli pdo_mysql pdo && a2enmod 


# Install composer
#RUN curl -sS https://getcomposer.org/installer | php

# Install Composer
#RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

